Para comenzar se debe instalar:
1.- Python en su versión 3.x y darle check en agregar pip (windows) https://www.python.org/downloads/
2.- En cmd ejecutar el comando pip install django (instalara la última versión 2.x)
3.- Para que funcione el imagefield (campo de imagen) se debe ejecutar el compando pip install Pillow
4.- Realizaremos en cmd las migraciones dentro de la carpeta root de nustro proyecto python manage.py makemigrations
5.- Ejecutamos en cmd las migraciones dentro de la carpeta root de nustro proyecto python manage.py migrate
6.- Luego ejecutamos el comando en cmd python manage runserver 
7.- Abrir su web browser con la ruta por defecto localhost:8000

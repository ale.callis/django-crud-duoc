from django.shortcuts import render, HttpResponse,HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

from .models import Mensaje
# Create your views here.

def index(request):
    if request.POST:
        print("post")
        nombre = request.POST.get("nombre",False)
        mensaje = request.POST.get("mensaje",False)
        foto = request.FILES.get("foto",False)
        if foto is False:
            foto = None
        msn = Mensaje(nombre=nombre,mensaje=mensaje,foto=foto)
        msn.save()
        messages.add_message(request, messages.SUCCESS, 'Creado con éxito')
    mensajes = Mensaje.objects.all()
    context= {'mensajes':mensajes}
    return render(request,"index.html",context)

def delete_mensaje(request,id):
    try:
        m = Mensaje.objects.get(id=id)
        m.delete()
        messages.add_message(request, messages.SUCCESS, 'Borrado con éxito')
    except Mensaje.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'No encontrado')

    return HttpResponseRedirect(reverse("index"))

def edit_mensaje(request,id):
    try:
        m = Mensaje.objects.get(id=id)
        mensajes = Mensaje.objects.all()
        context= {'mensajes':mensajes,'mensaje':m}
        if request.POST:
            nombre = request.POST.get("nombre",False)
            mensaje = request.POST.get("mensaje",False)
            foto = request.FILES.get("foto",False)
            borrar_foto = request.POST.get("borrar_foto",False)
            m.mensaje = mensaje 
            m.nombre = nombre
            if foto:
                m.foto = foto
            if borrar_foto:
                m.foto = None
            m.save()
            messages.add_message(request, messages.SUCCESS, 'Editado con éxito')
            return HttpResponseRedirect(reverse("index"))
        return render(request,"index.html",context)
    except Mensaje.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'No encontrado')
        return HttpResponseRedirect(reverse("index"))

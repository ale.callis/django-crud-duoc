from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
    path('',views.index,name="index"),
    path('delete_mensaje/<int:id>/',views.delete_mensaje,name="delete_mensaje"),
    path('edit_mensaje/<int:id>/',views.edit_mensaje, name="edit_mensaje")
]
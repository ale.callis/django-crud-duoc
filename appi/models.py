from django.db import models
from django.utils.deconstruct import deconstructible
import os
from uuid import uuid4


@deconstructible
class PathAndRename(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(self.path, filename)

path_and_rename_imagen = PathAndRename("fotos")

class Mensaje(models.Model):
    nombre = models.CharField(max_length=100)
    mensaje = models.TextField()
    foto = models.ImageField(upload_to=path_and_rename_imagen)